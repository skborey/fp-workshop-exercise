document.getElementById('fileinput').addEventListener('change', readFileFromEvent, false)

function readFileFromEvent (event) {
	var file = event.target.files[0]
	var r = new FileReader()
	r.onload = updateContent
	r.readAsBinaryString(file)
}

function logFileLoad(event) {
	var content = event.target.result
	console.log(content)
}

function updateContent(event) {
	// logFileLoad(event)
	setContent(parseMarkDown(event.target.result))
}

function setContent(content) {
	document.getElementById("main").innerHTML = content
}

function parseMarkDown (content) {
	// Do your work here

	var html = ""

	R.forEach(element => html += render(element), splitContentToElements(content))

	return html
}

function render(content) {
    var content = R.trim(content)

    if (isHeader(content)) return addHeaderTag(content)
    else return addParagraphTag(content)
}

function isHeader(content) { return (R.indexOf("#", R.trim(content)) === 0) }

function addHeaderTag(content) {

    var renderHeader = R.curry((level, content) => "<h" + level + ">" + content + "</h" + level + ">" )

    var level = findHeaderLevel(content)

    var cleanUpContent = R.curry((level, content) => R.drop(level, content))

    return R.compose(renderHeader(level), cleanUpContent(level))(content)

    function findHeaderLevel(content) {
        var headerLevels = ["######", "#####", "####", "###", "##", "#"]
        var level = 1

        if (R.indexOf(headerLevels[0], content) === 0) { level = 6 }
        else if (R.indexOf(headerLevels[1], content) === 0) { level = 5 }
        else if (R.indexOf(headerLevels[2], content) === 0) { level = 4 }
        else if (R.indexOf(headerLevels[3], content) === 0) { level = 3 }
        else if (R.indexOf(headerLevels[4], content) === 0) { level = 2 }
        else { level = 1 }

        return level
    }
}

function addParagraphTag(content) { return "<p>" + content + "</p>" }

function splitContentToElements(content) { return R.split("\n\n", content) }